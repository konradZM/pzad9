package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Term;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;

public class Termin extends java.lang.Object implements Serializable, Term {

    public PropertyChangeSupport propertyChangeSupport;

    private Date poczatek;
    private int czasTrwania;

    public Termin() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        poczatek = new Date();
    }

    public Termin(Date nowyPoczatek, int nowyCzasTrwania) {
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.poczatek = nowyPoczatek;
        this.czasTrwania = nowyCzasTrwania;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public int getDuration() {
        return this.czasTrwania;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            this.czasTrwania = duration;
        }
    }

    @Override
    public Date getEnd() {
        long t = poczatek.getTime();
        Date koniec = new Date(t + czasTrwania*60000);
        return koniec;
    }

    public Date getBegin() {
        return poczatek;
    }

    public void setBegin(Date begin) {
        this.poczatek = begin;
    }
}