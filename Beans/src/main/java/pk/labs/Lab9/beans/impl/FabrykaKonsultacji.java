package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.LinkedList;

public class FabrykaKonsultacji implements ConsultationListFactory{
    @Override
    public ConsultationList create() {
        return new ListaKonsultacji();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        if(deserialize){
            try{
                FileInputStream fileInputStream = new FileInputStream("konsultacje.xml");
                BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                XMLDecoder xmlDecoder = new XMLDecoder(bufferedInputStream);

                LinkedList<Consultation> deserializedConsultations = (LinkedList<Consultation>) xmlDecoder.readObject();

                return new ListaKonsultacji(deserializedConsultations);
            }
            catch(FileNotFoundException ex) {

            }
        }

        return this.create();
    }

    @Override
    public void save(ConsultationList consultationList) {
        LinkedList<Consultation> serializationList = new LinkedList<Consultation>();

        for(Consultation consultation: consultationList.getConsultation()){
            serializationList.add(consultation);
        }

        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(
                    new FileOutputStream("konsultacje.xml")));
            encoder.writeObject(serializationList);

            encoder.close();
        } catch (FileNotFoundException ex) {

        }
    }
}
