package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = Termin.class;
    public static Class<? extends Consultation> consultationBean = Konsultacja.class;
    public static Class<? extends ConsultationList> consultationListBean = ListaKonsultacji.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = FabrykaKonsultacji.class;
    
}
